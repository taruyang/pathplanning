#ifndef __LPASTAR_H__
#define __LPASTAR_H__

#include <vector>
#include <set>
#include "globalVariables.h"
#include "SearchAlgorithm.h"


class GridWorld; //forward declare class GridWorld to be able to create the friend functions later

class LpaStar: public SearchAlgorithm {

public:
    LpaStar(int rows, int cols, int heuristic = MANHATTAN); //constructor

    void Initialise(int startX, int startY, int goalX, int goalY);
    bool ComputeShortestPath();
    void UpdateHValues();
    void UpdateAllKeyValues();
    bool Move();

protected:
    void updateSuccesors(UnitCell* pCell, bool updateItself);
    void updateVertex(UnitCell* pCell);
    double calcRhs(UnitCell* center);
	double minValue(double g_, double rhs_);
	void calcKey(int x, int y);
    void calcKey(UnitCell* pCell);
    double calc_H(int x, int y);

};

#endif
