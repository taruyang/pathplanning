#include <stdio.h>
#include <iostream>
#include <stdlib.h>     /* calloc, exit, free */
#include <math.h>

#include "LPAstar.h"
#include "gridworld.h"


LpaStar::LpaStar(int rows, int cols, int heuristic):SearchAlgorithm(rows, cols, heuristic)
{
    _algorithm = LPASTAR_ALGORITHM;
}

void LpaStar::Initialise(int startX, int startY, int goalX, int goalY)
{
    SearchAlgorithm::Initialise(startX, startY, goalX, goalY);

    _queue.Clear();

	///START VERTEX
	_pStart->g = INF;
	_pStart->rhs = 0.0;

	///GOAL VERTEX
	_pGoal->g = INF;
	_pGoal->rhs = INF;

	/// Update the H values of all cells
	UpdateHValues();

	calcKey(_pStart);

	_queue.Insert(_pStart);
}

bool LpaStar::ComputeShortestPath()
{
	DBG_PRINT("[Lpa*][ComPath]\t Start \n");
	bool ret = false;
    startTimer();
    initStat();

	while(true) {
		calcKey(_pGoal);

		Key topKey;
		if(_queue.TopKey(topKey) == false) {
			DBG_PRINT("[Lpa*][ComPath]\t There is no item to handle even though it could not find the path \n");
			ret = false;
			break;
		}

		if((topKey < _pGoal->key) || (_pGoal->rhs != _pGoal->g)) {
            UnitCell* pTop = _queue.Pop();

			DBG_PRINT("[Lpa*][ComPath]\t Top priority cell \n");
			pTop->Print();

			if(pTop->g > pTop->rhs) {
				DBG_PRINT("[Lpa*][ComPath]\t The g value of top vertex is greater than rhs. So set rhs to g \n");
				pTop->g = pTop->rhs;
				updateSuccesors(pTop, false);
			}
			else {
				DBG_PRINT("[Lpa*][ComPath]\t The g value of top vertex is less than rhs. So set INF to g \n");
				pTop->g = INF;
				updateSuccesors(pTop, true);
			}

            _numStateExpansion++;
		}
		else {
			DBG_PRINT("[Lpa*][ComPath]\t Processes are successfully completed. \n");
			ret = true;
			break;
		}

		_queue.Print();
	}

    endTimer();
    if(_numStateExpansion)
        printStat();
	return ret;
}

void LpaStar::UpdateAllKeyValues()
{
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
		   calcKey(&_maze[i][j]);
		}
	}
}

void LpaStar::UpdateHValues()
{
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
		   _maze[i][j].h = calc_H(j, i);
		}
	}
}

bool LpaStar::Move()
{
    /// changes all unknown types to clear types.
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
	       char type = _maze[i][j].type;
            if((type == '9') || (type == '8')) {
                type = (type == '9') ? '1' : '0';
                UpdateCellType(j, i, type);
            }
		}
	}

    return ComputeShortestPath();

}
//================================= Protected methods =======================================
void LpaStar::updateSuccesors(UnitCell* pCell, bool updateItself)
{
	DBG_PRINT("[Lpa*][UdtVtx]\t update all successors %s of vertex (%d, %d) \n", (updateItself) ? "including vertex itself":"" , pCell->x, pCell->y);

    for(int i = 0; i < DIRECTIONS; i++){
        if((pCell->move[i] != nullptr) && (pCell->linkCost[i] != INF))
            updateVertex(pCell->move[i]);
    }

    if(updateItself)
        updateVertex(pCell);
}

void LpaStar::updateVertex(UnitCell* pCell)
{
	DBG_PRINT("[Lpa*][Udt %d,%d]\t update \n", pCell->x, pCell->y);

	if(pCell != _pStart){
		pCell->rhs = calcRhs(pCell);
		DBG_PRINT("[Lpa*][Udt %d,%d]\t vertex is not start vertex and calculated min rhs %.1f \n", pCell->x, pCell->y, pCell->rhs);
	}

	QueueIt it = _queue.Find(pCell);
	if(_queue.IsEnd(it) == false) {
		DBG_PRINT("[Lpa*][Udt %d,%d]\t vertex is in the Queue. So erase it from queue \n", pCell->x, pCell->y);
		_queue.Remove(it);
	}

	if(pCell->g != pCell->rhs) {
		DBG_PRINT("[Lpa*][Udt %d,%d]\t g(%.1f) of vertex is diff with rhs(%.1f). So calculates and insert again \n", pCell->x, pCell->y, pCell->g, pCell->rhs);
		calcKey(pCell);
		_queue.Insert(pCell);
	}
}

double LpaStar::calcRhs(UnitCell* pCell)
{
	double minRhs = INF;

	for(int i = 0; i < DIRECTIONS; i++) {
	    UnitCell* pNeighbor = pCell->move[i];
		if(pNeighbor != nullptr) {
            double cost = pNeighbor->g + pCell->linkCost[i];
			if(cost < minRhs)
				minRhs = cost;
		}
	}

	DBG_PRINT("[Lpa*][CalRhs]\t calculated min rhs value is %.1f \n", minRhs);
	return minRhs;
}

double LpaStar::minValue(double g_, double rhs_)
{
	if(g_ <= rhs_){
		return g_;
	} else {
		return rhs_;
	}
}

double LpaStar::calc_H(int x, int y)
{
    return calcDistance(_pGoal->x, _pGoal->y, x, y);
}

void LpaStar::calcKey(int x, int y)
{
	calcKey(&_maze[y][x]);
}

void LpaStar::calcKey(UnitCell *cell)
{
	double key1, key2;

	key2 = minValue(cell->g, cell->rhs);
	key1 = key2 + cell->h;

	cell->key.val[0] = key1;
	cell->key.val[1] = key2;
}

