#ifndef PRIORITYQUEUE_H_INCLUDED
#define PRIORITYQUEUE_H_INCLUDED

#include <vector>
#include <algorithm>
#include "globalVariables.h"

using namespace std;

struct Item
{
    Item(UnitCell* pCell):data(pCell){}

    UnitCell* data;

    bool operator<(const Item& rhs) const {
        return !(data->key < rhs.data->key);
    }
};

typedef vector<Item>            Queue;
typedef vector<Item>::iterator  QueueIt;

class PriorityQueue
{
public:
    PriorityQueue()
    {
        make_heap(_queue.begin(), _queue.end());
        _maxQLen = 0;
        _numVertexAccess = 0;
    };

    bool Insert(UnitCell* pCell)
    {
        if(pCell == nullptr) {
            PRINT("[Queue][Insert]\t Input Argument is not valid \n");
            return false;
        }

        pCell->key.x = pCell->x;
        pCell->key.y = pCell->y;

        DBG_PRINT("[Queue][Insert]\t A cell at (%d, %d) is inserted with key(%.1f, %.1f) \n", pCell->x, pCell->y, pCell->key.val[0], pCell->key.val[1]);
        Item cell(pCell);
        _queue.push_back(cell);
        push_heap(_queue.begin(), _queue.end());

        if((int)_queue.size() > _maxQLen){
            _maxQLen = (int)_queue.size();
        }

        _numVertexAccess++;

        Print();
        return true;
    }

    int GetMaxQLen(){
        return _maxQLen;
    }

    unsigned long GetNumVertexAssess(){
        return _numVertexAccess;
    }

    void InitStat(){
        _maxQLen = 0;
        _numVertexAccess = 0;
    }

    bool TopKey(Key& key)
    {
        if(_queue.empty()) {
			DBG_PRINT("[Queue][TopKey]\t There is no item to handle \n");
            return false;
        }

        Item cell = _queue.front();

		key = cell.data->key;
		_numVertexAccess++;

        return true;
    }

    bool IsEnd(QueueIt it)
    {
        return it == _queue.end();
    }

    UnitCell* Pop()
    {
        if(_queue.empty()) {
			DBG_PRINT("[Queue][Pop]\t There is no item to handle \n");
            return nullptr;
        }

        Item cell = _queue.front();

        pop_heap(_queue.begin(), _queue.end());
        _queue.pop_back();
        _numVertexAccess++;

        Print();
		return cell.data;
    }

    void Print()
    {
        DBG_PRINT("[Queue][Print]\t Print all items in Queue ================================ \n");
        for(QueueIt it = _queue.begin(); it != _queue.end(); ++it) {
            Item item = *it;
            item.data->Print();
        }
        DBG_PRINT("[Queue][Print]\t ========================================================= \n");
    }

    void Clear()
    {
        DBG_PRINT("[Queue][Clear]\t ========================================================= \n");
        _queue.clear();
    }

    QueueIt Find(UnitCell* pCell)
    {
        if(pCell == nullptr) {
            PRINT("[Queue][Find]\t Input Argument is not valid \n");
            return _queue.end();
        }

        QueueIt it;

        for(it = _queue.begin(); it != _queue.end(); ++it) {
            _numVertexAccess++;
            if((*it).data == pCell)
                break;
        }
        return it;
    }

    void Remove(QueueIt it)
    {
        _queue.erase(it);
        _numVertexAccess++;
        make_heap(_queue.begin(), _queue.end());
    }

private:
    int            _maxQLen;
    unsigned long  _numVertexAccess;
    Queue _queue;
};

#endif // PRIORITYQUEUE_H_INCLUDED
