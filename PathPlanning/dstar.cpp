#include <stdio.h>
#include <iostream>
#include <stdlib.h>     /* calloc, exit, free */
#include <math.h>

#include "dstar.h"
#include "gridworld.h"

DStar::DStar(int rows, int cols, int heuristic):SearchAlgorithm(rows, cols, heuristic)
{
    _algorithm = DSTAR_ALGORITHM;
}

void DStar::Initialise(int startX, int startY, int goalX, int goalY)
{
    SearchAlgorithm::Initialise(startX, startY, goalX, goalY);

    _pLast = _pStart;

    _km = 0.f;

	_pGoal->rhs = 0.f;

	UpdateHValues();

	calcKey(_pGoal);

	_queue.Insert(_pGoal);
}

bool DStar::ComputeShortestPath()
{
	DBG_PRINT("[D*][ComPath]\t Start \n");
	bool ret = false;
    startTimer();
    initStat();

	while(true) {
		calcKey(_pStart);

		Key kOld;
		if(_queue.TopKey(kOld) == false) {
			PRINT("[D*][ComPath]\t There is no item to handle even though it could not find the path \n");
			ret = false;
			break;
		}

		if((kOld < _pStart->key) || (_pStart->rhs != _pStart->g)) {
            UnitCell* pTop = _queue.Pop();
            calcKey(pTop);
			DBG_PRINT("[D*][ComPath]\t Top priority cell \n");
			pTop->Print();

			if(kOld < pTop->key) {
				DBG_PRINT("[D*][ComPath]\t The kOld value of top vertex is less than calculated new key. Insert again to Queue \n");
                _queue.Insert(pTop);
			}
			else if(pTop->g > pTop->rhs) {
				DBG_PRINT("[D*][ComPath]\t The g value of top vertex is greater than rhs. So set rhs to g \n");
				pTop->g = pTop->rhs;
				updatePredecessor(pTop, false);
			}
			else {
				DBG_PRINT("[D*][ComPath]\t The g value of top vertex is less than rhs. So set INF to g \n");
				pTop->g = INF;
				updatePredecessor(pTop, true);
			}

            _numStateExpansion++;
		}
		else {
			DBG_PRINT("[D*][ComPath]\t Processes are successfully completed. \n");
			ret = true;
			break;
		}

		_queue.Print();
	}

	endTimer();
	if(_numStateExpansion)
        printStat();
	return ret;
}

bool DStar::Move()
{
    if(_pStart == _pGoal){
        PRINT("[D*][Move]\t Already arrived at the goal \n");
        return false;
    }

    if(_pStart->g == INF){
        PRINT("[D*][Move]\t There is no known path. \n");
        return false;
    }

    /// Move to Sstart
    _pStart->type = '0';
    _pStart = findMinSucc(_pStart);
    _pStart->type = '6';
    /// Update all H values
    UpdateHValues();

    bool updated = updateEdgeCosts(_pStart);
    if(updated)
        ComputeShortestPath();

    return true;
}

void DStar::UpdateAllKeyValues()
{
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
		   calcKey(&_maze[i][j]);
		}
	}
}

void DStar::UpdateHValues()
{
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
		   _maze[i][j].h = calc_H(j, i);
		}
	}
}
//================================= Protected methods =======================================
void DStar::updatePredecessor(UnitCell* pCell, bool updateItself)
{
	DBG_PRINT("[D*][UdtVtx]\t update all successors %s of vertex (%d, %d) \n", (updateItself) ? "including vertex itself":"" , pCell->x, pCell->y);

    for(int i = 0; i < DIRECTIONS; i++){
        if((pCell->move[i] != nullptr) && (pCell->linkCost[i] != INF))
            updateVertex(pCell->move[i]);
    }

    if(updateItself)
        updateVertex(pCell);
}

void DStar::updateVertex(UnitCell* pCell)
{
	DBG_PRINT("[D*][Udt %d,%d]\t update \n", pCell->x, pCell->y);

	if(pCell != _pGoal){
		pCell->rhs = calcRhs(pCell);
		DBG_PRINT("[D*][Udt %d,%d]\t vertex is not start vertex and calculated min rhs %.1f \n", pCell->x, pCell->y, pCell->rhs);
	}

	QueueIt it = _queue.Find(pCell);
	if(_queue.IsEnd(it) == false) {
		DBG_PRINT("[D*][Udt %d,%d]\t vertex is in the Queue. So erase it from queue \n", pCell->x, pCell->y);
		_queue.Remove(it);
	}

	if(pCell->g != pCell->rhs) {
		DBG_PRINT("[D*][Udt %d,%d]\t g(%.1f) of vertex is diff with rhs(%.1f). So calculates and insert again \n", pCell->x, pCell->y, pCell->g, pCell->rhs);
		calcKey(pCell);
		_queue.Insert(pCell);
	}
}

bool DStar::updateEdgeCosts(UnitCell* pCell)
{
    bool updated = false;
    for(int i = 0; i < DIRECTIONS; i++){
        UnitCell* pNeighbor = pCell->move[i];
        if((pNeighbor != nullptr) && (pNeighbor->type == '9' || pNeighbor->type == '8')) {
            if(updated == false) {
                _km += _pLast->h;
                _pLast = _pStart;
                updated = true;
            }

            char type = (pNeighbor->type == '9') ? '1' : '0';
            UpdateCellType(pNeighbor->x, pNeighbor->y, type);
        }
    }

    return updated;
}

UnitCell* DStar::findMinSucc(UnitCell* pCell)
{
	double      minRhs = INF;
	UnitCell*   pMinNeighbor = nullptr;

	for(int i = 0; i < DIRECTIONS; i++) {
	    UnitCell* pNeighbor = pCell->move[i];
		if(pNeighbor != nullptr) {
            double cost = pNeighbor->g + pCell->linkCost[i];
			if(cost < minRhs) {
				minRhs = cost;
				pMinNeighbor = pNeighbor;
			}
		}
	}

    return pMinNeighbor;
}

double DStar::calcRhs(UnitCell* pCell)
{
	double minRhs = INF;

	for(int i = 0; i < DIRECTIONS; i++) {
	    UnitCell* pNeighbor = pCell->move[i];
		if(pNeighbor != nullptr) {
            double cost = pNeighbor->g + pCell->linkCost[i];
			if(cost < minRhs)
				minRhs = cost;
		}
	}

	DBG_PRINT("[D*][CalRhs]\t calculated min rhs value is %.1f \n", minRhs);
	return minRhs;
}

double DStar::minValue(double g_, double rhs_)
{
	if(g_ <= rhs_){
		return g_;
	} else {
		return rhs_;
	}
}

double DStar::calc_H(int x, int y)
{
    return calcDistance(_pStart->x, _pStart->y, x, y);
}

void DStar::calcKey(int x, int y)
{
	calcKey(&_maze[y][x]);
}

void DStar::calcKey(UnitCell *cell)
{
	double key1, key2;

	key2 = minValue(cell->g, cell->rhs);
	key1 = key2 + cell->h + _km;

	cell->key.val[0] = key1;
	cell->key.val[1] = key2;
}

