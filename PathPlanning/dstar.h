#ifndef __DSTAR_H__
#define __DSTAR_H__

#include <vector>
#include <set>
#include "globalVariables.h"
#include "SearchAlgorithm.h"


class GridWorld; //forward declare class GridWorld to be able to create the friend functions later

class DStar: public SearchAlgorithm {

public:
    DStar(int rows, int cols, int heuristic = MANHATTAN); //constructor

    void Initialise(int startX, int startY, int goalX, int goalY);
    bool ComputeShortestPath();
    void UpdateHValues();
    void UpdateAllKeyValues();
    bool Move();

protected:
    void updatePredecessor(UnitCell* pCell, bool updateItself);
    void updateVertex(UnitCell* pCell);
    bool updateEdgeCosts(UnitCell* pCell);
    UnitCell* findMinSucc(UnitCell* pCell);
    double calcRhs(UnitCell* center);
	double minValue(double g_, double rhs_);
	void calcKey(int x, int y);
    void calcKey(UnitCell* pCell);
    double calc_H(int x, int y);

protected:
    UnitCell* _pLast;
    double _km;
};



#endif
